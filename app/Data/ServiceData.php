<?php

namespace App\Data;

use GuzzleHttp\Client;

class ServiceData
{
    protected $client;
    protected $parameters;

    public function __construct()
    {

        $url = 'https://apiperu.dev';
        $token = 'eb1ff5c644d4f0832416f34b089b99d01437bf21a496f89b61d90a60a1624353';

        $this->client = new Client(['base_uri' => $url]);
        $this->parameters = [
            'http_errors' => false,
            'connect_timeout' => 10,
            'verify' => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ];
    }

    public function service($type, $number)
    {
        $res = $this->client->request('GET', '/api/'.$type.'/'.$number, $this->parameters);
        $response = json_decode($res->getBody()->getContents(), true);

        $res_data = [];
        if($response['success']) {
            $data = $response['data'];
            if($type === 'dni') {
                $department_id = '';
                $province_id = null;
                $district_id = null;
                $address = null;
                if(key_exists('source', $response) && $response['source'] === 'apiperu.dev') {
                    if(strlen($data['ubigeo_sunat']))  {
                        $department_id = $data['ubigeo'][0];
                        $province_id = $data['ubigeo'][1];
                        $district_id = $data['ubigeo'][2];
                        $address = $data['direccion'];
                    }
                } else {
                    $department_id = $data['ubigeo'][0];
                    $province_id = $data['ubigeo'][1];
                    $district_id = $data['ubigeo'][2];
                    $address = $data['direccion'];
                }

                $res_data = [
                    'name' => $data['nombre_completo'],
                    'trade_name' => '',
                    'location_id' => $district_id,
                    'address' => $address,
                    'department_id' => $department_id,
                    'province_id' => $province_id,
                    'district_id' => $district_id,
                    'condition' => '',
                    'state' => '',
                ];
            }

            if($type === 'ruc') {
                $address = '';
                $department_id = null;
                $province_id = null;
                $district_id = null;
                if(key_exists('source', $response) && $response['source'] === 'apiperu.dev') {
                    if(strlen($data['ubigeo_sunat']))  {
                        $department_id = $data['ubigeo'][0];
                        $province_id = $data['ubigeo'][1];
                        $district_id = $data['ubigeo'][2];
                        $address = $data['direccion'];
                    }
                } else {
                    $department_id = $data['ubigeo'][0];
                    $province_id = $data['ubigeo'][1];
                    $district_id = $data['ubigeo'][2];
                    $address = $data['direccion'];
                }

                $res_data = [
                    'name' => $data['nombre_o_razon_social'],
                    'trade_name' => '',
                    'address' => $address,
                    'department_id' => $department_id,
                    'province_id' => $province_id,
                    'district_id' => $district_id,
                    'condition' => $data['condicion'],
                    'state' => $data['estado'],
                ];
            }
            $response['data'] = $res_data;
        }

        return $response;
    }

}
