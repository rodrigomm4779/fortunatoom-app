<?php


namespace App\Filters\App\Cliente;

use App\Filters\App\Traits\SearchFilterClientes;
use App\Filters\App\Traits\StatusFilterFT;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class ClienteFilter extends FilterBuilder
{
    use SearchFilterClientes, StatusFilterFT;

    public function searchSelect($descripcion = null)
    {
        $this->whereClause('nombres_apellidos', $descripcion);
    }

}
