<?php


namespace App\Filters\App\Sorteo;


use App\Filters\App\Traits\DateRangeFilter;
use App\Filters\App\Traits\SearchFilterFT;
use App\Filters\App\Traits\StatusFilterFT;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class SorteoFilter extends FilterBuilder
{
    use SearchFilterFT, StatusFilterFT;

    public function searchSelect($descripcion = null)
    {
        $this->whereClause('descripcion', $descripcion);
    }

}
