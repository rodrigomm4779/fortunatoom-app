<?php


namespace App\Filters\App\Talonario;

use App\Filters\App\Traits\SearchFilterFT;
use App\Filters\App\Traits\StatusFilterFT;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class TalonarioFilter extends FilterBuilder
{
    use SearchFilterFT, StatusFilterFT;

    public function searchSelect($descripcion = null)
    {
        $this->whereClause('descripcion', $descripcion);
    }

}
