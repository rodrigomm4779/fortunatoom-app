<?php


namespace App\Filters\App\Traits;


trait SearchFilterClientes
{
    public function search($search = null)
    {
        $this->singleSearch($search, 'nombres_apellidos');
    }
}
