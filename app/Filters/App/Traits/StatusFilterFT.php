<?php


namespace App\Filters\App\Traits;


trait StatusFilterFT
{
    public function status($status = null)
    {
        $this->singleSearch($status, 'status');
    }
}