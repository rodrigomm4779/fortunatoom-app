<?php


namespace App\Filters\App\Venta;


use App\Filters\App\Traits\DateRangeFilter;
use App\Filters\App\Traits\SearchFilterVentas;
use App\Filters\App\Traits\StatusFilter;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class VentaFilter extends FilterBuilder
{
    use SearchFilterVentas, StatusFilter, DateRangeFilter;

    public function searchSelect($descripcion = null)
    {
        $this->whereClause('cliente.nombres_apellidos', $descripcion);
    }

}
