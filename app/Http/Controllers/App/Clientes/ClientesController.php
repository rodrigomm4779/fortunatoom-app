<?php

namespace App\Http\Controllers\App\Clientes;

use App\Filters\App\Cliente\ClienteFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\ClienteRequest as Request;
use App\Models\App\Cliente\Cliente;
use App\Notifications\App\Crud\CrudNotification;
use App\Services\App\Cliente\ClienteService;

class ClientesController extends Controller
{
    /**
     * ClientesController constructor.
     * @param ClienteService $service
     * @param ClienteFilter $filter
     */
    public function __construct(ClienteService $service, ClienteFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->service
            // ->select('full_data')
            ->orderBy('id')
            ->filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        return view('demo-crud.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = $this->service->save();

        notify()
            ->on('row_created')
            ->with($cliente)
            ->send(CrudNotification::class);

        return created_responses('data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        $cliente = $this->service->update($cliente);

        notify()
            ->on('row_updated')
            ->with($cliente)
            ->send(CrudNotification::class);

        return updated_responses('data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Crud $cliente)
    {
        if ($this->service->delete($cliente)) {

            notify()
                ->on('row_deleted')
                ->with((object)$cliente->toArray())
                ->send(CrudNotification::class);

            return deleted_responses('data');
        }
        return failed_responses();
    }
    public function getNameFromDatatable()
    {
        return $this->service->getName();
    }
}
