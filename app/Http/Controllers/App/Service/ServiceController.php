<?php

namespace App\Http\Controllers\App\Service;

use App\Http\Controllers\Controller;
use App\Data\ServiceData;

class ServiceController extends Controller
{
    public function service($type, $number)
    {
        return (new ServiceData)->service($type, $number);
    }

    public function exchange($date)
    {
        return (new ServiceData)->exchange($date);
    }
}
