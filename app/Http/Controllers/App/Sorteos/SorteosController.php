<?php

namespace App\Http\Controllers\App\Sorteos;

use App\Filters\App\Sorteo\SorteoFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\SorteoRequest as Request;
use App\Models\App\Sorteo\Sorteo;
use App\Notifications\App\Crud\CrudNotification;
use App\Services\App\Sorteo\SorteoService;

class SorteosController extends Controller
{
    /**
     * SorteosController constructor.
     * @param SorteoService $service
     * @param SorteoFilter $filter
     */
    public function __construct(SorteoService $service, SorteoFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->service
            ->with('talonario:id,numero_inicio,numero_fin,precio_venta_unitario,status')
            ->orderBy('id')
            ->filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
        
        // return (new AppRoleFilter(
        //     $this->service
        //         ->with('users:id,first_name,last_name,email', 'users.profilePicture')
        //         ->orderBy('id')
        //         ->filters($this->filter)
        // ))->filter()
        //     ->paginate(request()->get('per_page', 10));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        return view('demo-crud.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sorteo = $this->service->save();

        // notify()
        //     ->on('row_created')
        //     ->with($sorteo)
        //     ->send(CrudNotification::class);

        return created_responses('data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sorteo $sorteo)
    {
        $sorteo = $this->service->update($sorteo);

        notify()
            ->on('row_updated')
            ->with($sorteo)
            ->send(CrudNotification::class);

        return updated_responses('data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Sorteo $sorteo)
    {
        if ($this->service->delete($sorteo)) {

            // notify()
            //     ->on('row_deleted')
            //     ->with((object)$sorteo->toArray())
            //     ->send(CrudNotification::class);

            return deleted_responses('data');
        }
        return failed_responses();
    }
    public function getNameFromDatatable()
    {
        return $this->service->getName();
    }
}
