<?php

namespace App\Http\Controllers\App\Talonarios;

use App\Filters\App\Talonario\TalonarioFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\TalonarioRequest as Request;
use App\Models\App\Talonario\Talonario;
use App\Notifications\App\Crud\CrudNotification;
use App\Services\App\Talonario\TalonarioService;

class TalonariosController extends Controller
{
    /**
     * TalonariosController constructor.
     * @param TalonarioService $service
     * @param TalonarioFilter $filter
     */
    public function __construct(TalonarioService $service, TalonarioFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->service
            // ->with('talonario:id,numero_inicio,numero_fin,precio_venta_unitario,status')
            ->orderBy('id')
            ->filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        return view('demo-crud.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $talonario = $this->service->save();

        notify()
            ->on('row_created')
            ->with($talonario)
            ->send(CrudNotification::class);

        return created_responses('data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Talonario $talonario)
    {
        $talonario = $this->service->update($talonario);

        notify()
            ->on('row_updated')
            ->with($talonario)
            ->send(CrudNotification::class);

        return updated_responses('data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Crud $talonario)
    {
        if ($this->service->delete($talonario)) {

            notify()
                ->on('row_deleted')
                ->with((object)$talonario->toArray())
                ->send(CrudNotification::class);

            return deleted_responses('data');
        }
        return failed_responses();
    }
    public function getNameFromDatatable()
    {
        return $this->service->getName();
    }
}
