<?php

namespace App\Http\Controllers\App\Ventas;

use Illuminate\Support\Facades\DB;
use App\Filters\App\Venta\VentaFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\App\VentaRequest as Request; 

use App\Models\App\Venta\Venta;
use App\Models\App\VentaItem\VentaItem;
use App\Models\App\Talonario\Talonario;
use App\Models\App\Sorteo\Sorteo;

use App\Notifications\App\Crud\CrudNotification;
use App\Services\App\Venta\VentaService;

class VentasController extends Controller
{
    /**
     * VentasController constructor.
     * @param VentaService $service
     * @param VentaFilter $filter
     */
    public function __construct(VentaService $service, VentaFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            return $this->service
            ->with('cliente', 'items', 'user:id,first_name,last_name,email')
            ->orderBy('id')
            ->filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
        } else {
            return $this->service
            ->with('cliente', 'items', 'user:id,first_name,last_name,email')
            ->where('user_id', auth()->user()->id)
            ->orderBy('id')
            ->filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
        }
        
        
        
        // return (new AppRoleFilter(
        //     $this->service
        //         ->with('users:id,first_name,last_name,email', 'users.profilePicture')
        //         ->orderBy('id')
        //         ->filters($this->filter)
        // ))->filter()
        //     ->paginate(request()->get('per_page', 10));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        return view('demo-crud.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
		try {
            // Agregando la habitación a la lista de productos
            $venta = new Venta();
            $venta->cliente_id = $request->cliente_id;
            $venta->user_id = auth()->user()->id;
            $venta->sorteo_id = $request->sorteo_id;
            $venta->token = $request->token;
            $venta->total_items = $request->total_items;
            $venta->status = $request->status;
            $venta->total = $request->total;
            $venta->save();

            $sorteo = Sorteo::with('talonario')->findOrFail($request->sorteo_id);
            $last_number_ticket =  (int)$sorteo->talonario->numero_actual;//5000

            //Generamos detalle de venta con codigos de ticket y guardamos
            // 5000; 5000 < 5002 ; 5000
            for($i = $last_number_ticket; $i < ((int)$request->total_items + (int)$last_number_ticket); $i++){
                $ventaItem = new VentaItem();
                $ventaItem->venta_id = $venta->id;
                $ventaItem->talonario_id = $sorteo->talonario->id;
                $ventaItem->codigo_ticket = $i;
                $ventaItem->cantidad = 1;
                $ventaItem->valor_unitario = $sorteo->talonario->precio_venta_unitario;
                $ventaItem->total = $sorteo->talonario->precio_venta_unitario;
                $ventaItem->status = $request->status;
                $ventaItem->save();
            }


            //Actualizamos numero actual de talonario
            $talonario = Talonario::findOrFail($sorteo->talonario->id);
            $talonario->numero_actual = $last_number_ticket + (int)$request->total_items;
            $talonario->save();

            // $sorteo = $this->service->save();

            // notify()
            //     ->on('row_created')
            //     ->with($sorteo)
            //     ->send(CrudNotification::class);

            // return created_responses('data');
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Venta registrada de forma correcta.',
                'data' => Venta::with('items', 'cliente')->findOrFail($venta->id),
            ], 200);

        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json([
                'success' => true,
                'message' => 'No se puede procesar su transacción. Detalles: ' . $th->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->with('cliente', 'items', 'user:id,first_name,last_name,email')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $venta = Venta::findOrFail($id);
		$venta->fill($request->only('status'));
		$venta->save();
        // $venta = $this->service->update($venta);

        // notify()
        //     ->on('row_updated')
        //     ->with($venta)
        //     ->send(CrudNotification::class);

        return updated_responses('data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Venta $venta)
    {
        if ($this->service->delete($venta)) {

            // notify()
            //     ->on('row_deleted')
            //     ->with((object)$venta->toArray())
            //     ->send(CrudNotification::class);

            return deleted_responses('data');
        }
        return failed_responses();
    }
    public function getNameFromDatatable()
    {
        return $this->service->getName();
    }
}
