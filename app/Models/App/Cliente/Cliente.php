<?php

namespace App\Models\App\Cliente;

// use App\Models\App\Sorteo;

use App\Models\App\AppModel;
use App\Models\App\Traits\ClienteValidationRules;

class Cliente extends AppModel
{
    use ClienteValidationRules;
    protected $fillable = ['tipo_doc_identidad', 'numero_doc_identidad', 'nombres_apellidos', 'celular', 'ciudad', 'status'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [
        'full_data',
    ];

    public function getFullDataAttribute()
    {
        return $this->numero_doc_identidad
            ? $this->numero_doc_identidad.' - '.$this->nombres_apellidos
            : $this->nombres_apellidos;
    }

    // public function sorteo()
    // {
    //     return $this->belongsTo(Sorteo::class)->with('id');
    // }
}
