<?php

namespace App\Models\App\Sorteo;

use App\Models\App\Talonario\Talonario;

use App\Models\App\AppModel;
use App\Models\App\Traits\SorteoValidationRules;

class Sorteo extends AppModel
{
    use SorteoValidationRules;
    protected $fillable = [ 'talonario_id', 'hora', 'dia', 'descripcion', 'status'];

    /**
     * @return HasMany
     */
    public function talonario()
    {
        return $this->hasOne(Talonario::class, 'id', 'talonario_id');
    }
}
