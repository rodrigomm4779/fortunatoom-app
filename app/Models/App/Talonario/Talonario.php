<?php

namespace App\Models\App\Talonario;

use App\Models\App\Sorteo;

use App\Models\App\AppModel;
use App\Models\App\Traits\CrudValidationRules;

class Talonario extends AppModel
{
    use CrudValidationRules;
    protected $fillable = ['numero_inicio','numero_fin','stock','total_vendidos','precio_venta_unitario', 'is_active'];

    public function sorteo()
    {
        return $this->belongsTo(Sorteo::class)->with('id');
    }
}
