<?php


namespace App\Models\App\Traits;


trait ClienteValidationRules
{
    public function createdRules()
    {
        return [
            'tipo_doc_identidad' => '',
            'numero_doc_identidad' => 'required',
            'nombres_apellidos' => 'required|min:2|max:195',
            'celular' => 'nullable',
            'ciudad' => 'nullable',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }

    public function updatedRules()
    {
        return [
            'tipo_doc_identidad' => '',
            'numero_doc_identidad' => 'required',
            'nombres_apellidos' => 'required|min:2|max:195',
            'celular' => 'nullable',
            'ciudad' => 'nullable',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }
}