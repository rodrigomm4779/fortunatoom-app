<?php


namespace App\Models\App\Traits;


trait SorteoValidationRules
{
    public function createdRules()
    {
        return [
            'talonario_id' => 'required',
            'dia' => 'nullable',
            'hora' => 'nullable',
            'descripcion' => 'required|min:2|max:195',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }

    public function updatedRules()
    {
        return [
            'talonario_id' => 'required',
            'dia' => 'nullable',
            'hora' => 'nullable',
            'descripcion' => 'required|min:2|max:195',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }
}