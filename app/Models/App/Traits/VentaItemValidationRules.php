<?php


namespace App\Models\App\Traits;


trait VentaItemValidationRules
{
    public function createdRules()
    {
        return [
            'venta_id' => 'required',
            'talonario_id' => 'required',
            'codigo_ticket' => 'required',
            'cantidad' => 'required',
            'valor_unitario' => 'required',
            'total' => 'required',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }

    public function updatedRules()
    {
        return [
            'venta_id' => 'required',
            'talonario_id' => 'required',
            'codigo_ticket' => 'required',
            'cantidad' => 'required',
            'valor_unitario' => 'required',
            'total' => 'required',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }
}