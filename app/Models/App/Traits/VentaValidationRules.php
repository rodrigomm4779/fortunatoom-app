<?php


namespace App\Models\App\Traits;


trait VentaValidationRules
{
    public function createdRules()
    {
        return [
            'cliente_id' => 'required',
            'sorteo_id' => 'required',
            'token' => 'nullable',
            'total_items' => 'nullable',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }

    public function updatedRules()
    {
        return [
            'cliente_id' => 'required',
            'sorteo_id' => 'required',
            'token' => 'nullable',
            'total_items' => 'nullable',
            'status' => 'nullable|in:active,inactive,invited',
        ];
    }
}