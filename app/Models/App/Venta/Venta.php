<?php

namespace App\Models\App\Venta;

use App\Models\App\Cliente\Cliente;
use App\Models\App\VentaItem\VentaItem;
use App\Models\Core\Auth\User;

use App\Models\App\AppModel;
use App\Models\App\Traits\VentaValidationRules;

class Venta extends AppModel
{
    use VentaValidationRules;
    protected $fillable = ['cliente_id', 'user_id', 'sorteo_id', 'token', 'total_items', 'status', 'total'];
     
    /**
     * @return HasOne
     */
    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'cliente_id');
    }

    /**
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(VentaItem::class, 'venta_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
}
