<?php

namespace App\Models\App\VentaItem;

use App\Models\App\AppModel;
use App\Models\App\Traits\VentaItemValidationRules;

class VentaItem extends AppModel
{
    use VentaItemValidationRules;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venta_items';

    protected $fillable = ['venta_id', 'talonario_id', 'codigo_ticket', 'cantidad', 'valor_unitario', 'total', 'status'];
     
}
