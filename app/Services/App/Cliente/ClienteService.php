<?php


namespace App\Services\App\Cliente;

use App\Models\App\Cliente\Cliente;
use App\Services\App\AppService;

class ClienteService extends AppService
{
    public function __construct(Cliente $cliente)
    {
        $this->model = $cliente;
    }

    /**
     * Get only name from Talonario model
     * @return \Illuminate\Support\Collection
     */
    public function getName()
    {
        return $this->model->select('descripcion')->get();
    }

    /**
     * Update Talonario service
     * @param Talonario $cliente
     * @return Talonario
     */
    public function update(Talonario $cliente)
    {
        $cliente->fill(request()->all());

        $this->model = $cliente;

        $cliente->save();

        return $cliente;
    }

    /**
     * Delete Talonario service
     * @param Talonario $cliente
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Talonario $cliente)
    {
        return $cliente->delete();
    }
}
