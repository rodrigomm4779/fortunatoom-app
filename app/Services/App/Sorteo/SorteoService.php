<?php


namespace App\Services\App\Sorteo;

use App\Models\App\Sorteo\Sorteo;
use App\Services\App\AppService;

class SorteoService extends AppService
{
    public function __construct(Sorteo $sorteo)
    {
        $this->model = $sorteo;
    }

    /**
     * Get only name from Sorteo model
     * @return \Illuminate\Support\Collection
     */
    public function getName()
    {
        return $this->model->select('descripcion')->get();
    }

    /**
     * Update Sorteo service
     * @param Sorteo $sorteo
     * @return Sorteo
     */
    public function update(Sorteo $sorteo)
    {
        $sorteo->fill(request()->all());

        $this->model = $sorteo;

        $sorteo->save();

        return $sorteo;
    }

    /**
     * Delete Sorteo service
     * @param Sorteo $sorteo
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Sorteo $sorteo)
    {
        return $sorteo->delete();
    }
}
