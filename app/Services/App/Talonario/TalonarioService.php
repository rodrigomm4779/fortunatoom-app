<?php


namespace App\Services\App\Talonario;

use App\Models\App\Talonario\Talonario;
use App\Services\App\AppService;

class TalonarioService extends AppService
{
    public function __construct(Talonario $talonario)
    {
        $this->model = $talonario;
    }

    /**
     * Get only name from Talonario model
     * @return \Illuminate\Support\Collection
     */
    public function getName()
    {
        return $this->model->select('descripcion')->get();
    }

    /**
     * Update Talonario service
     * @param Talonario $talonario
     * @return Talonario
     */
    public function update(Talonario $talonario)
    {
        $talonario->fill(request()->all());

        $this->model = $talonario;

        $talonario->save();

        return $talonario;
    }

    /**
     * Delete Talonario service
     * @param Talonario $talonario
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Talonario $talonario)
    {
        return $talonario->delete();
    }
}
