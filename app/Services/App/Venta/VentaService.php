<?php


namespace App\Services\App\Venta;

use App\Models\App\Venta\Venta;
use App\Services\App\AppService;

class VentaService extends AppService
{
    public function __construct(Venta $venta)
    {
        $this->model = $venta;
    }

    /**
     * Get only name from Venta model
     * @return \Illuminate\Support\Collection
     */
    public function getName()
    {
        return $this->model->select('descripcion')->get();
    }

    /**
     * Update Venta service
     * @param Venta $venta
     * @return Venta
     */
    public function update(Venta $venta)
    {
        $venta->fill(request()->all());

        $this->model = $venta;

        $venta->save();

        return $venta;
    }

    /**
     * Delete Venta service
     * @param Venta $venta
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Venta $venta)
    {
        return $venta->delete();
    }
}
