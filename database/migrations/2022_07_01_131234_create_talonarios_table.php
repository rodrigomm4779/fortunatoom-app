<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTalonariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talonarios', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->nullable();
            $table->integer('numero_inicio')->default(0);
            $table->integer('numero_fin')->default(0);
            $table->integer('numero_actual')->default(0);
            $table->integer('stock')->default(0);
            $table->integer('total_vendidos')->default(0);
            $table->decimal('precio_venta_unitario', 12, 2);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talonarios');
    }
}
