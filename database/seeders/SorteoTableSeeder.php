<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\App\Sorteo\Sorteo;

class SorteoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sorteo = [
            [

                'talonario_id' => 1,
                'descripcion' => 'Sorteo Fortunatoom I',
                'status' => 'active',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];
        Sorteo::query()->insert($sorteo);
    }
}
