<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\App\Talonario\Talonario;

class TalonarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $talonario = [
            [
                'numero_inicio' => 5000,
                'numero_fin' => 10000,
                'stock' => 5000,
                'total_vendidos' => 0,
                'precio_venta_unitario' => 10.00,
                'status' => 'active',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];
        Talonario::query()->insert($talonario);
    }
}
