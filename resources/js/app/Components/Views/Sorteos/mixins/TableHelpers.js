export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: 'Descripción',
                    type: 'text',
                    key: 'descripcion',
            
                },
                {
                    title: 'Fecha',
                    type: 'text',
                    key: 'dia',
            
                },
                {
                    title: 'Hora',
                    type: 'text',
                    key: 'hora',
            
                },
                {
                    title: 'Estado',
                    type: 'custom-html',
                    key: 'status',
                    isVisible: true,
                    modifier: (value) => {
                        if (value) {
                            let ClassName = 'danger';

                            if (value === 'active') ClassName = `success`;
                            else if (value === 'inactive') ClassName = `warning`;

                            return `<span class="badge badge-sm badge-pill badge-${ClassName}">${value}</span>`;
                        }
                    }
                },
                {
                    title: 'Precio ticket',
                    type: 'custom-html',
                    key: 'talonario',
                    modifier: (value) => {
                        return `<span>S/. ${value.precio_venta_unitario}</span>`
            
                    }
                },
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}