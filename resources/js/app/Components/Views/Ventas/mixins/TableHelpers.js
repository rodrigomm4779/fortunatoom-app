export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: 'ID',
                    type: 'text',
                    key: 'id',
            
                },
                {
                    title: 'Cliente',
                    type: 'custom-html',
                    key: 'cliente',
                    modifier: (value) => {
                        return `<span>${value.nombres_apellidos}</span>`
            
                    }
                },
                {
                    title: 'Celular',
                    type: 'custom-html',
                    key: 'cliente',
                    modifier: (value) => {
                        return `<span>${value.celular}</span>`
            
                    }
                },
                {
                    title: 'Estado',
                    type: 'custom-html',
                    key: 'status',
                    isVisible: true,
                    modifier: (value) => {
                        if (value) {
                            let ClassName = 'danger';
                            let TextValue = 'Vendido';

                            if (value === 'active') {
                                ClassName = `success`;
                                TextValue = 'Validado';
                            }
                            else if (value === 'inactive') ClassName = `warning`;

                            return `<span class="badge badge-sm badge-pill badge-${ClassName}">${TextValue}</span>`;
                        }
                    }
                },
                {
                    title: 'Cantidad',
                    type: 'custom-html',
                    key: 'items',
                    modifier: (value) => {
                        let countItems = value.filter(item => item).length;

                        return `<span>${countItems}</span>`
            
                    }
                },
                {
                    title: 'Total venta',
                    type: 'custom-html',
                    key: 'total',
                    modifier: (value) => {
                        return `<span>S/. ${value}</span>`
            
                    }
                },
                {
                    title: 'Vendedor',
                    type: 'custom-html',
                    key: 'user',
                    modifier: (value) => {
                        return `<span>${value.full_name}</span>`
            
                    }
                }, 
            ],
            actionObj : {
                title: 'Acciones',
                type: 'action',
                isVisible: this.uscan.validate_ventas,
            }
        }
    },
}