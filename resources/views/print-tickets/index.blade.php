@extends('auth-layouts.auth')

@section('title', 'Ventas')

@section('contents')
    <div id="app">
        <print-tickets-add-index :venta="{{json_encode($venta)}}"></print-tickets-add-index>
    </div>
    <script>
        window.localStorage.setItem('app-languages',
            JSON.stringify(
                <?php echo json_encode(include resource_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . (app()->getLocale() ?? 'en') . DIRECTORY_SEPARATOR . 'default.php')?>
            )
        );
    </script>
@endsection