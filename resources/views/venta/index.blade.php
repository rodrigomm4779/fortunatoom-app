@extends('layouts.app')

@section('title', 'Ventas')

@section('contents')
    @php
        $uscan = array(
            'is_admin' => auth()->user()->isAdmin(), 
            'validate_ventas' => auth()->user()->can('validate_ventas'), 
        );
    @endphp
    <ventas-index :uscan="{{json_encode($uscan)}}"></ventas-index>
@endsection