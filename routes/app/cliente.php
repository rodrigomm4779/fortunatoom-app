<?php

use App\Http\Controllers\App\Clientes\ClientesController;

// Route::group(['prefix' => 'cliente'], function () {
//     Route::view('/view', 'cliente.index');
// });

// Default dashboard
// Route::get('/sorteo/name', [ClientesController::class, 'getNameFromDatatable']);
Route::resource('cliente', ClientesController::class);
