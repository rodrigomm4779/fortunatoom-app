<?php

use App\Http\Controllers\App\Service\ServiceController;

// Default dashboard
Route::get('/service/{type}/{number}', [ServiceController::class, 'service']);
