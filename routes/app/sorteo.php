<?php

use App\Http\Controllers\App\Sorteos\SorteosController;

Route::group(['prefix' => 'sorteo'], function () {
    Route::view('/view', 'sorteo.index');
});

// Default dashboard
Route::get('/sorteo/name', [SorteosController::class, 'getNameFromDatatable']);
Route::resource('sorteo', SorteosController::class);
