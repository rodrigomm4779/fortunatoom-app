<?php

use App\Http\Controllers\App\Talonarios\TalonariosController;

// Route::group(['prefix' => 'talonario'], function () {
//     Route::view('/view', 'talonario.index');
// });

// Default dashboard
// Route::get('/talonario/name', [TalonariosController::class, 'getNameFromDatatable']);
Route::resource('talonario', TalonariosController::class);
