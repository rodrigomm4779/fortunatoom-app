<?php

use App\Http\Controllers\App\Ventas\VentasController;

Route::group(['prefix' => 'venta'], function () {
    Route::view('/view', 'venta.index');
    Route::view('/add', 'venta.add');
});

// Default dashboard
Route::get('/venta/name', [VentasController::class, 'getNameFromDatatable']);
Route::resource('venta', VentasController::class);
